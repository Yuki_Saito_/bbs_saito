var gulp = require('gulp');

var sass = require('gulp-sass');
gulp.task('sass',function(){ 
 gulp.src('scss/**/*.scss')
 .pipe(sass())
 .pipe(gulp.dest('css'));
});

gulp.task('bs-reload', function () {
    browserSync.reload();
});

 gulp.task('watch' , function () {
    gulp.watch("./*.html",            ['bs-reload']);
    gulp.watch("./css/*.css", ['bs-reload']);
    gulp.watch("./*.js",   ['bs-reload']);
});
